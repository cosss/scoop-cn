<div align="center">

# scoop-cn

</div>

## 安装

在PowerShell中运行

``` powershell
iwr -useb https://gitee.com/RubyMetric/scoop-cn/raw/main/install.ps1 | iex
```

**注意: 安装完毕后需要注销并重新登陆或重启，以便在终端可以访问到`scoop`命令**

如果报错请先运行以下命令
``` powershell
Set-ExecutionPolicy RemoteSigned -scope CurrentUser
```

<br>

## 维护

- `<2023-07-31>`

    该脚本依然可以运行，没有问题

- `<2021-10-31>`

    上游正在开发新的安装方式，对于安装结果来说，同此仓库的旧安装方式暂时没有区别。若您发现通过此仓库方式安装失败，请您及时汇报情况，协助我们找到并解决问题，感谢。
